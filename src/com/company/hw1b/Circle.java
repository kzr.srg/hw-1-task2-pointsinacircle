package com.company.hw1b;

public class Circle {
    private final Point center;
    private final Double radius;

    public Circle(Point center, Double radius) {
        this.center = center;
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public Double getRadius() {
        return radius;
    }

    public boolean containsPoint(Point p) {
        double distance = center.distanceTo(p);
        return distance < radius;

    }
}
