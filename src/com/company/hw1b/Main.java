package com.company.hw1b;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        TextService textService = new TextService();
        Scanner scanner = new Scanner(System.in);
        textService.setScanner(scanner);
        PointList pointList = new PointList();

        byte answer;

        do{
           double x = textService.scanDoubleValue(textService.GetTextX());
           double y = textService.scanDoubleValue(textService.GetTextY());

            pointList.addPoint(new Point(x,y));

            while (true) {
                textService.showNextMessage();
                if (scanner.hasNextByte()){
                     answer = scanner.nextByte();
                    if (answer ==1 || answer == 2){
                        textService.showYourChoice(answer);
                        break;
                    } else {
                        textService.showErrorMessage();
                        continue;
                    }
                }
                textService.showErrorMessage();
                scanner.next();
            }

        } while (answer == 1);

        double cx = textService.scanDoubleValue(textService.GetTextCenterX());
        double cy = textService.scanDoubleValue(textService.GetTextCenterY());
        double radius = textService.scanDoubleValue(textService.GetTextRadius());

        Circle circle =new Circle(new Point(cx,cy),radius);
        textService.showSplit();

        textService.showInfoCircle(circle);
        textService.showContainsPointsCircle(circle,pointList);

        textService.showSplit();
    }
}
