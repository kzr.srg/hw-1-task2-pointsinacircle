package com.company.hw1b;

public class Point {
    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distanceTo(Point p){
        double dx = p.x - x;
        double dy = p.y - y;
        double distanceSquare = dx*dx + dy*dy;
        return Math.sqrt(distanceSquare);
    }

}
