package com.company.hw1b;

import java.util.Arrays;

public class PointList {

    private int length;

    private Point[] pointList = new Point[0];

    public void addPoint(Point p) {
        pointList = Arrays.copyOf(pointList, pointList.length + 1);
        pointList[pointList.length - 1] = p;
        length ++;
    }
    public int getLength() {
        return length;
    }

    public Point getPointToIndex(int i) {
        return pointList[i];
    }

}


