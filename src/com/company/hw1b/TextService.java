package com.company.hw1b;

import java.util.Scanner;

public class TextService {

    private Scanner scanner;

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }
    public String GetTextX(){
        return "Введите координаты точки (x):";
    }
    public String GetTextY(){
        return "Введите координаты точки (y):";
    }
    public String GetTextCenterX(){
        return "Введите координаты центра окружности (x):";
    }
    public String GetTextCenterY(){
        return "Введите координаты центра окружности (y):";
    }
    public String GetTextRadius(){
        return "Введите радиус окружности:";
    }
    public void showSplit() {
        System.out.println("########################################################");
    }
    public void showNextMessage() {
        System.out.println("Желаете добавить еще? (1-да 2-нет)");
    }
    public void showErrorMessage() {
        System.out.println("Введено неверное значение. Повторите ввод");
    }
    public void showYourChoice(byte i) {
        System.out.println("Ваш выбор "+i);
    }
    public void showInfoCircle(Circle circle) {
        Point center = circle.getCenter();
        System.out.println("Центр окружности(x,y):" + center.getX() + "," + center.getY() + ". Радиус:" + circle.getRadius());
    }
    public void showContainsPointsCircle(Circle circle, PointList pointList) {
        int pointNumber = 0;

        for (int i = 0; i < pointList.getLength(); i++) {
            Point currentPoint = pointList.getPointToIndex(i);
            if (circle.containsPoint(currentPoint)) {
                pointNumber = i + 1;
                System.out.println("Точка № " + pointNumber + " (" + currentPoint.getX() + "," + currentPoint.getY() + ") находится в пределах окружности");
            }
        }
        if (pointNumber == 0) {
            System.out.println("Нет точек в пределах окружности");
        }
    }
    public double scanDoubleValue( String message){
        System.out.print(message);
        while (true) {
            if (scanner.hasNextDouble()){
                return scanner.nextDouble();
            } else {
                showErrorMessage();
                System.out.print(message);
                scanner.next();
            }
        }
    }
}


